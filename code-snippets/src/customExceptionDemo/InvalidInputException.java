package customExceptionDemo;

public class InvalidInputException extends Exception {

	public InvalidInputException(String message) {
		super(message);
		// System.out.println(message);
	}

	private static final long serialVersionUID = 1L;

}
